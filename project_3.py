# -*- coding: utf-8 -*-
"""
Created on Fri Sep 22 20:25:03 2017

@author: slava
"""


import sys
from PyQt5.QtWidgets import QApplication, QWidget,\
QFileDialog, QAction, QMainWindow, QMessageBox, QLabel,\
QCheckBox, QGridLayout, QSlider
from PyQt5 import QtCore
from PyQt5.QtGui import QPixmap, QColor, QImage
 

class App(QMainWindow):
 
    def __init__(self):
        
        super(App, self).__init__()
        
        self.title = 'Filter for Picture'
        self.left = 200
        self.top = 200
        self.width = 640
        self.height = 480
        self.initUI()
 
    def initUI(self):
        
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        # Save Picture
        saveFile = QAction('&Save Picture', self)
        saveFile.setShortcut('Ctrl+S')
        saveFile.setStatusTip('Save')
        saveFile.triggered.connect(self.save_file)


        # Open Picture 
        openPict = QAction('&Open Picture', self)
        openPict.setShortcut('Ctrl+O')
        openPict.setStatusTip('Open Picture')
        openPict.triggered.connect(self.file_open)
        
        
        
        widget = QWidget() #  set Work space 
        self.setCentralWidget(widget) # add work space
        self.layout = QGridLayout() # Разметка
        widget.setLayout(self.layout) # add разметка
       
       
        self.label1 = QLabel() # label for change pict
        self.layout.addWidget(self.label1, 0, 0)
        
        #add new widget to 
        menu_filter = QWidget()
        
        #Add checkbox convert color to Black and white
        self.sld = QSlider(QtCore.Qt.Horizontal, self)
        self.sld.setFocusPolicy(QtCore.Qt.NoFocus)
        self.sld.setGeometry(30, 40, 100, 30)
        self.sld.valueChanged[int].connect(self.change_value)
        self.my_value = 0
        
        
        self.checkbox_0 = QCheckBox('Color -> B & W', self)
        self.checkbox_0.stateChanged.connect(self.filt_conv_col_bw)

        # Add checbox conver color to grey
        self.checkbox_1 = QCheckBox('Color -> Grey', self)
        self.checkbox_1.stateChanged.connect(self.filt_color_grey)
        
        self.gridlayout = QGridLayout()
        menu_filter.setLayout(self.gridlayout)
        
        self.gridlayout.addWidget( self.checkbox_0, 0, 0)
        self.gridlayout.addWidget(self.checkbox_1, 1, 0)
        self.gridlayout.addWidget(self.sld, 0, 1)
        
        self.layout.addWidget(menu_filter, 0, 1)
        
        # Add and set status Bar
        self.statusBar()
        mainMenu = self.menuBar()
        self.statusBar().showMessage('Ready')
        
        
        #Add filemenu
        fileMenu = mainMenu.addMenu('&File')
        fileMenu.addAction(openPict)
        fileMenu.addAction(saveFile)
        
        
#        self.openFileNameDialog()
#        self.openFileNamesDialog()
#        self.saveFileDialog()
 
        self.show()
    
    def file_open(self):
        
        file_name = QFileDialog.getOpenFileName(self,'Open Picture')
        self.pixmap = QPixmap(file_name[0])
        self.pixmap_copy = QPixmap(file_name[0])
        
        pict = self.pixmap.scaled(self.pixmap.width()//2,self.pixmap.height()//2)
        self.label1.setPixmap(pict)
        
    def save_file(self):
        
        name = QFileDialog.getSaveFileName(self, 'Save File')
        file = open(name,'w')
        text = self.textEdit.toPlainText()
        file.write(text)
        file.close()
     
    # MY FILTERS
    
    # 1st filter: Convert color pict to black and white pict    
    def filt_conv_col_bw(self, state):
        
        if state == QtCore.Qt.Checked:
            
            picture = self.pixmap.toImage()
            new_pict = QImage(self.pixmap.width(), self.pixmap.height(),  QImage.Format_ARGB32)

            for i in range(self.pixmap.width()):
                for j in range(self.pixmap.height()):
                    position = QtCore.QPoint(i,j)
                    color = QColor.fromRgb(picture.pixel(position))
                    
                    red, green, blue =  int(str(color.red())), int(str(color.green())), int(str(color.blue()))
                    s = red + green + blue
                    
                    if ( s > ((255 + self.my_value) * 3 // 2)):
                        
                        red, green, blue = 0, 0, 0
                        
                    else:
                        
                        red, green, blue = 255, 255, 255
                    color_new = QColor(red, green, blue)
                    new_pict.setPixelColor(position,color_new)
                    
#                    print (red, green, blue)
#                    print ( str(position), int(str(color.red())), str(color.green()), str(color.blue()))
#            self.pixmap.fromImage(new_pict)
            new_picture = self.pixmap.fromImage(new_pict)
            self.pixmap = new_picture
            self.label1.setPixmap(new_picture.scaled(self.pixmap.width()//2, self.pixmap.height()//2))
        else: 
            self.label1.setPixmap(self.pixmap_copy.scaled(self.pixmap.width()//2, self.pixmap.height()//2))
          
      
    def filt_color_grey(self,state):
        
        if state == QtCore.Qt.Checked:
            
            picture = self.pixmap.toImage()
            new_pict = QImage(self.pixmap.width(), self.pixmap.height(),  QImage.Format_ARGB32)

            for i in range(self.pixmap.width()):
                for j in range(self.pixmap.height()):
                    position = QtCore.QPoint(i,j)
                    color = QColor.fromRgb(picture.pixel(position))
                    
                    red, green, blue =  int(str(color.red())), int(str(color.green())), int(str(color.blue()))
                    s = red + green + blue
                    s = s // 3
                    color_new = QColor(s, s, s)
                    new_pict.setPixelColor(position,color_new)
                    
#                    print (red, green, blue)
#                    print ( str(position), int(str(color.red())), str(color.green()), str(color.blue()))
#            self.pixmap.fromImage(new_pict)
            new_picture = self.pixmap.fromImage(new_pict)
            self.pixmap = new_picture
            self.label1.setPixmap(new_picture.scaled(self.pixmap.width()//2, self.pixmap.height()//2))
        else: 
            
            self.label1.setPixmap(self.pixmap_copy.scaled(self.pixmap.width()//2, self.pixmap.height()//2))
      
    def change_value(self, value):
        
        print(self.my_value, value)
        self.my_value = value
        print (self.my_value, value)
        
        
        
        
#    def openFileNameDialog(self):    
#        options = QFileDialog.Options()
#        options |= QFileDialog.DontUseNativeDialog
#        fileName, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","All Files (*);;Python Files (*.py)", options=options)
#        if fileName:
#            print(fileName)
#    
    
    def closeEvent(self, event):
        
        reply = QMessageBox.question(self, 'Message', "Вы уверены, что хотите\
 закрыть приложени?", QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        
        if reply == QMessageBox.Yes:
            event.accept()
        else:
            event.ignore()
 
    
    
    
if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = App()
    sys.exit(app.exec_())